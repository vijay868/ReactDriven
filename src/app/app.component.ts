import { Component } from '@angular/core';
import { Power } from './models/power';
import { FormGroup, 
         FormControl, 
         Validators, 
         FormBuilder, 
         AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  powerArr: Array<Power> = [];
  sampleForm: FormGroup;

  constructor(private formBuilder: FormBuilder){
    this.sampleForm = formBuilder.group({
      name: new FormControl(null, Validators.required),
      email: new FormControl(null, [Validators.required, this.EmailDomainValidator]),
      alterEgo: new FormControl(null, Validators.required),
      power: new FormControl(null, Validators.required),
      temperature: new FormControl(null, this.TemperatureValidator.bind(this)),
      phone: new FormControl(null, [this.PhoneValidator, Validators.maxLength(10), Validators.minLength(10)])
    });

    this.powerArr.push(
      { id: 1, name: 'Really Smart' },
      { id: 2, name: 'Super Flexible' },
      { id: 3, name: 'Super Hot' },
      { id: 4, name: 'Weather Changer' });
  }

  TemperatureValidator(control: AbstractControl): {[s: string]: boolean} {
    const group = control.parent;
    if(group){      
      if(group.controls['power'].value == 4){        
        return Validators.required(control);
      }
    }    
    return null;
  }

  EmailDomainValidator(control: FormControl): {[s: string] : any}{
    let email = control.value;
    if(email && email.indexOf('@') != -1){
      let [_, domain] = email.split('@');
      if(domain !== 'ivycomptech.com'){
        return {
          emailDomain: {
            parsedDomain: domain
          }
        }
      }
    }
    return null;
  }

  PhoneValidator(control: FormControl): {[s: string]: boolean}{
    if(control.value != null && control.value.length != 0){
      return Validators.required(control);
    }
    return null;
  }

  onSubmit(): void {
    console.log(this.sampleForm);
  }
}
